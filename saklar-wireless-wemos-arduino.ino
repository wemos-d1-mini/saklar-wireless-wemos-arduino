#include <ESP8266WiFi.h>
#include <aREST.h>
#include <aREST_UI.h>

aREST_UI rest = aREST_UI();

const char* ssid = "Redmi";
const char* password = "Kero.007.123";

#define LISTEN_PORT           80

WiFiServer server(LISTEN_PORT);

int ledControl(String command);

void setup(void) {
  Serial.begin(115200);

  rest.title("Kontrol Stop Kontak");

  rest.button(6);
  rest.button(7);

  pinMode(D6, OUTPUT);
  digitalWrite(D6, LOW);
  pinMode(D7, OUTPUT);
  digitalWrite(D7, LOW);

  rest.function("led", ledControl);

  rest.set_id("1");
  rest.set_name("esp8266");

  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());

}

void loop() {
  // Handle REST calls
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  while (!client.available()) {
    delay(1);
  }
  rest.handle(client);

}

int ledControl(String command) {
  // Print command
  Serial.println(command);

  // Get state from command
  int state = command.toInt();

  digitalWrite(6, state);
  digitalWrite(7, state);
  return 1;
}
